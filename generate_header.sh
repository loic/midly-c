#!/bin/bash

set -ex

INTERNAL=("SystemCommon MidiStream")

cbindgen --config cbindgen.toml --crate midly-c --output midly.h

sed -i "9 i // hack: forward declarations to workaround cbindgen limitations" midly.h
for int in $INTERNAL
do
    sed -i "10 i typedef struct MidlyInternal$int MidlyInternal$int;" midly.h
done
