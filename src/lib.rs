//! [midly](https://github.com/negamartin/midly) C API

#![no_std]

use core::ptr::drop_in_place;

use midly::{live, stream};

/// Error status
#[repr(C)]
#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Status {
    Ok,
    NotRightVariant,
    InvalidMidiInput,
    NotEnoughSpace,
    AllocFailure,
}

/// Allocator struct
#[repr(C)]
pub struct AllocOperations {
    alloc: extern "C" fn(core::ffi::c_uint) -> *mut core::ffi::c_void,
    dealloc: extern "C" fn(*mut core::ffi::c_void),
}

// hack: avoid wrong with cbindgen
use stream::MidiStream as InternalMidiStream;

/// Midi Stream
#[repr(C)]
pub struct MidiStream {
    alloc_ops: AllocOperations,
    stream: *mut InternalMidiStream,
}

/// Live event type
#[repr(C)]
pub enum LiveEventType {
    Midi,
    Common,
    Realtime,
}

/// Live event inner
#[repr(C)]
pub union LiveEventInner<'a> {
    midi: MidiEvent,
    common: SystemCommon<'a>,
    realtime: SystemRealtime,
}

/// Live event
#[repr(C)]
pub struct LiveEvent<'a> {
    type_: LiveEventType,
    inner: LiveEventInner<'a>,
}

/// Midi message - note off
#[derive(Copy, Clone)]
#[repr(C)]
pub struct NoteOff {
    key: u8,
    vel: u8,
}

/// Midi message - note on
#[derive(Copy, Clone)]
#[repr(C)]
pub struct NoteOn {
    key: u8,
    vel: u8,
}

/// Midi message - after touch
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Aftertouch {
    key: u8,
    vel: u8,
}

/// Midi message - controller
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Controller {
    controller: u8,
    value: u8,
}

/// Midi message - program change
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ProgramChange {
    program: u8,
}

/// Midi message - channel aftertouch
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ChannelAftertouch {
    vel: u8,
}

/// Midi message - pitch bend
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PitchBend {
    bend: u16,
}

/// Midi message
#[derive(Copy, Clone)]
#[repr(C)]
pub union MidiMessage {
    note_off: NoteOff,
    note_on: NoteOn,
    aftertouch: Aftertouch,
    controller: Controller,
    program_change: ProgramChange,
    channel_aftertouch: ChannelAftertouch,
    pitch_bend: PitchBend,
}

/// Midi message type
#[derive(Copy, Clone)]
#[repr(C)]
pub enum MidiMessageType {
    NoteOff,
    NoteOn,
    Aftertouch,
    Controller,
    ProgramChange,
    ChannelAftertouch,
    PitchBend,
}

/// Midi event
#[derive(Copy, Clone)]
#[repr(C)]
pub struct MidiEvent {
    channel: u8,
    msg_type: MidiMessageType,
    msg: MidiMessage,
}

// hack: avoid wrong with cbindgen
use live::SystemCommon as InternalSystemCommon;

/// System common event
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SystemCommon<'a> {
    com: *mut InternalSystemCommon<'a>,
}

/// System realtime type
#[derive(Copy, Clone)]
#[repr(C)]
pub enum SystemRealtimeType {
    TimingClock,
    Start,
    Continue,
    Stop,
    ActiveSensing,
    Reset,
    Undefined,
}

/// System realtime event
#[derive(Copy, Clone)]
#[repr(C)]
pub struct SystemRealtime {
    type_: SystemRealtimeType,
    /// Only defined for [SystemRealtimeType::Undefined]
    und: u8,
}

impl<'a, 'b> From<&'a mut live::LiveEvent<'b>> for LiveEvent<'b> {
    fn from(value: &'a mut live::LiveEvent<'b>) -> Self {
        match value {
            live::LiveEvent::Midi { channel, message } => LiveEvent {
                type_: LiveEventType::Midi,
                inner: LiveEventInner {
                    midi: MidiEvent {
                        channel: (*channel).into(),
                        msg_type: (*message).into(),
                        msg: (*message).into(),
                    },
                },
            },
            live::LiveEvent::Common(com) => LiveEvent {
                type_: LiveEventType::Common,
                inner: LiveEventInner {
                    common: SystemCommon { com },
                },
            },
            live::LiveEvent::Realtime(rt) => LiveEvent {
                type_: LiveEventType::Realtime,
                inner: LiveEventInner {
                    realtime: (*rt).into(),
                },
            },
        }
    }
}

impl From<live::SystemRealtime> for SystemRealtime {
    fn from(value: live::SystemRealtime) -> Self {
        match value {
            live::SystemRealtime::TimingClock => SystemRealtime {
                type_: SystemRealtimeType::TimingClock,
                und: 0xff,
            },
            live::SystemRealtime::Start => SystemRealtime {
                type_: SystemRealtimeType::Start,
                und: 0,
            },
            live::SystemRealtime::Continue => SystemRealtime {
                type_: SystemRealtimeType::Continue,
                und: 0,
            },
            live::SystemRealtime::Stop => SystemRealtime {
                type_: SystemRealtimeType::Stop,
                und: 0,
            },
            live::SystemRealtime::ActiveSensing => SystemRealtime {
                type_: SystemRealtimeType::ActiveSensing,
                und: 0,
            },
            live::SystemRealtime::Reset => SystemRealtime {
                type_: SystemRealtimeType::Reset,
                und: 0,
            },
            live::SystemRealtime::Undefined(und) => SystemRealtime {
                type_: SystemRealtimeType::Undefined,
                und,
            },
        }
    }
}

impl From<midly::MidiMessage> for MidiMessage {
    fn from(value: midly::MidiMessage) -> Self {
        match value {
            midly::MidiMessage::NoteOff { key, vel } => MidiMessage {
                note_off: NoteOff {
                    key: key.into(),
                    vel: vel.into(),
                },
            },
            midly::MidiMessage::NoteOn { key, vel } => MidiMessage {
                note_on: NoteOn {
                    key: key.into(),
                    vel: vel.into(),
                },
            },
            midly::MidiMessage::Aftertouch { key, vel } => MidiMessage {
                aftertouch: Aftertouch {
                    key: key.into(),
                    vel: vel.into(),
                },
            },
            midly::MidiMessage::Controller { controller, value } => MidiMessage {
                controller: Controller {
                    controller: controller.into(),
                    value: value.into(),
                },
            },
            midly::MidiMessage::ProgramChange { program } => MidiMessage {
                program_change: ProgramChange {
                    program: program.into(),
                },
            },
            midly::MidiMessage::ChannelAftertouch { vel } => MidiMessage {
                channel_aftertouch: ChannelAftertouch { vel: vel.into() },
            },
            midly::MidiMessage::PitchBend { bend } => MidiMessage {
                pitch_bend: PitchBend {
                    bend: bend.0.into(),
                },
            },
        }
    }
}

impl From<midly::MidiMessage> for MidiMessageType {
    fn from(value: midly::MidiMessage) -> Self {
        match value {
            midly::MidiMessage::NoteOff { .. } => MidiMessageType::NoteOff,
            midly::MidiMessage::NoteOn { .. } => MidiMessageType::NoteOn,
            midly::MidiMessage::Aftertouch { .. } => MidiMessageType::Aftertouch,
            midly::MidiMessage::Controller { .. } => MidiMessageType::Aftertouch,
            midly::MidiMessage::ProgramChange { .. } => MidiMessageType::ProgramChange,
            midly::MidiMessage::ChannelAftertouch { .. } => MidiMessageType::ChannelAftertouch,
            midly::MidiMessage::PitchBend { .. } => MidiMessageType::PitchBend,
        }
    }
}

impl LiveEvent<'_> {
    unsafe fn try_into_midly(&self) -> Result<live::LiveEvent, Status> {
        match self.type_ {
            LiveEventType::Midi => self.inner.midi.try_into_midly(),
            LiveEventType::Common => todo!(),
            LiveEventType::Realtime => self.inner.realtime.try_into_midly(),
        }
    }
}

impl SystemRealtime {
    unsafe fn try_into_midly(&self) -> Result<live::LiveEvent, Status> {
        let SystemRealtime { type_, und } = self;

        let sys = match type_ {
            SystemRealtimeType::TimingClock => live::SystemRealtime::TimingClock,
            SystemRealtimeType::Start => live::SystemRealtime::Start,
            SystemRealtimeType::Continue => live::SystemRealtime::Continue,
            SystemRealtimeType::Stop => live::SystemRealtime::Stop,
            SystemRealtimeType::ActiveSensing => live::SystemRealtime::ActiveSensing,
            SystemRealtimeType::Reset => live::SystemRealtime::Reset,
            SystemRealtimeType::Undefined => live::SystemRealtime::Undefined(*und),
        };

        Ok(live::LiveEvent::Realtime(sys))
    }
}

impl MidiEvent {
    unsafe fn try_into_midly(&self) -> Result<live::LiveEvent, Status> {
        let MidiEvent {
            channel,
            msg_type,
            msg,
        } = self;

        let msg = match msg_type {
            MidiMessageType::NoteOff => midly::MidiMessage::NoteOff {
                key: unsafe {
                    msg.note_off
                        .key
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
                vel: unsafe {
                    msg.note_off
                        .vel
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::NoteOn => midly::MidiMessage::NoteOn {
                key: unsafe {
                    msg.note_on
                        .key
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
                vel: unsafe {
                    msg.note_on
                        .vel
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::Aftertouch => midly::MidiMessage::Aftertouch {
                key: unsafe {
                    msg.aftertouch
                        .key
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
                vel: unsafe {
                    msg.aftertouch
                        .vel
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::Controller => midly::MidiMessage::Controller {
                controller: unsafe {
                    msg.controller
                        .controller
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
                value: unsafe {
                    msg.controller
                        .value
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::ProgramChange => midly::MidiMessage::ProgramChange {
                program: unsafe {
                    msg.program_change
                        .program
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::ChannelAftertouch => midly::MidiMessage::ChannelAftertouch {
                vel: unsafe {
                    msg.channel_aftertouch
                        .vel
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                },
            },
            MidiMessageType::PitchBend => midly::MidiMessage::PitchBend {
                bend: midly::PitchBend(unsafe {
                    msg.pitch_bend
                        .bend
                        .try_into()
                        .map_err(|_| Status::InvalidMidiInput)?
                }),
            },
        };

        Ok(live::LiveEvent::Midi {
            channel: (*channel).into(),
            message: msg,
        })
    }
}

/// Create a new [MidiStream]
#[no_mangle]
pub extern "C" fn midly_stream_new(alloc_ops: AllocOperations, stream: *mut MidiStream) -> Status {
    let alloced = (alloc_ops.alloc)(core::mem::size_of::<stream::MidiStream>() as _)
        as *mut stream::MidiStream;
    if alloced.is_null() {
        return Status::AllocFailure;
    };
    // SAFETY: the pointer is non-null and has been corectly allocated
    unsafe {
        *stream = MidiStream {
            alloc_ops,
            stream: alloced,
        }
    };
    Status::Ok
}

/// Drop (and free) a [MidiStream]
#[no_mangle]
pub extern "C" fn midly_stream_drop(stream: *mut MidiStream) {
    // SAFETY: TODO
    let MidiStream { alloc_ops, stream } = unsafe { &mut (*stream) };
    // SAFETY: TODO
    unsafe { drop_in_place(stream) };
    (alloc_ops.dealloc)(stream.cast());
}

/// Feed new data in a [MidiStream]
#[no_mangle]
pub extern "C" fn midly_stream_feed(
    stream: *mut MidiStream,
    data: *const u8,
    len: usize,
    handle_ev: unsafe extern "C" fn(*const LiveEvent<'_>),
) {
    // SAFETY: TODO
    let stream = unsafe { &mut (*(*stream).stream) };
    // SAFETY: TODO
    let bytes = unsafe { core::slice::from_raw_parts(data, len) };

    stream.feed(bytes, |ref mut live_event| {
        let live_event = live_event.into();
        unsafe { handle_ev(&live_event) }
    })
}

/// Write a [MidiEvent] into a buffer
#[no_mangle]
pub extern "C" fn midly_live_event_write(
    live_event: *const LiveEvent,
    buf: *mut u8,
    len: usize,
) -> Status {
    let Ok(midi_event) = (unsafe {(&*live_event).try_into_midly() }) else {
        return Status::InvalidMidiInput;
    };

    let mut slice = unsafe { core::slice::from_raw_parts_mut(buf, len) };

    match midi_event.write(&mut slice) {
        Ok(_) => Status::Ok,
        Err(_err) => Status::NotEnoughSpace,
    }
}
