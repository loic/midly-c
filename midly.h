// SPDX-License-Identifier: MIT

#ifndef MIDLY_C_H
#define MIDLY_C_H

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
// hack: forward declarations to workaround cbindgen limitations
typedef struct MidlyInternalMidiStream MidlyInternalMidiStream;
typedef struct MidlyInternalSystemCommon MidlyInternalSystemCommon;

/**
 * Live event type
 */
typedef enum {
    MIDLY_LIVE_EVENT_TYPE_MIDI,
    MIDLY_LIVE_EVENT_TYPE_COMMON,
    MIDLY_LIVE_EVENT_TYPE_REALTIME,
} MidlyLiveEventType;

/**
 * Midi message type
 */
typedef enum {
    MIDLY_MIDI_MESSAGE_TYPE_NOTE_OFF,
    MIDLY_MIDI_MESSAGE_TYPE_NOTE_ON,
    MIDLY_MIDI_MESSAGE_TYPE_AFTERTOUCH,
    MIDLY_MIDI_MESSAGE_TYPE_CONTROLLER,
    MIDLY_MIDI_MESSAGE_TYPE_PROGRAM_CHANGE,
    MIDLY_MIDI_MESSAGE_TYPE_CHANNEL_AFTERTOUCH,
    MIDLY_MIDI_MESSAGE_TYPE_PITCH_BEND,
} MidlyMidiMessageType;

/**
 * Error status
 */
typedef enum {
    MIDLY_STATUS_OK,
    MIDLY_STATUS_NOT_RIGHT_VARIANT,
    MIDLY_STATUS_INVALID_MIDI_INPUT,
    MIDLY_STATUS_NOT_ENOUGH_SPACE,
    MIDLY_STATUS_ALLOC_FAILURE,
} MidlyStatus;

/**
 * System realtime type
 */
typedef enum {
    MIDLY_SYSTEM_REALTIME_TYPE_TIMING_CLOCK,
    MIDLY_SYSTEM_REALTIME_TYPE_START,
    MIDLY_SYSTEM_REALTIME_TYPE_CONTINUE,
    MIDLY_SYSTEM_REALTIME_TYPE_STOP,
    MIDLY_SYSTEM_REALTIME_TYPE_ACTIVE_SENSING,
    MIDLY_SYSTEM_REALTIME_TYPE_RESET,
    MIDLY_SYSTEM_REALTIME_TYPE_UNDEFINED,
} MidlySystemRealtimeType;

/**
 * Allocator struct
 */
typedef struct {
    void *(*alloc)(unsigned int);
    void (*dealloc)(void*);
} MidlyAllocOperations;

/**
 * Midi Stream
 */
typedef struct {
    MidlyAllocOperations alloc_ops;
    MidlyInternalMidiStream *stream;
} MidlyMidiStream;

/**
 * Midi message - note off
 */
typedef struct {
    uint8_t key;
    uint8_t vel;
} MidlyNoteOff;

/**
 * Midi message - note on
 */
typedef struct {
    uint8_t key;
    uint8_t vel;
} MidlyNoteOn;

/**
 * Midi message - after touch
 */
typedef struct {
    uint8_t key;
    uint8_t vel;
} MidlyAftertouch;

/**
 * Midi message - controller
 */
typedef struct {
    uint8_t controller;
    uint8_t value;
} MidlyController;

/**
 * Midi message - program change
 */
typedef struct {
    uint8_t program;
} MidlyProgramChange;

/**
 * Midi message - channel aftertouch
 */
typedef struct {
    uint8_t vel;
} MidlyChannelAftertouch;

/**
 * Midi message - pitch bend
 */
typedef struct {
    uint16_t bend;
} MidlyPitchBend;

/**
 * Midi message
 */
typedef union {
    MidlyNoteOff note_off;
    MidlyNoteOn note_on;
    MidlyAftertouch aftertouch;
    MidlyController controller;
    MidlyProgramChange program_change;
    MidlyChannelAftertouch channel_aftertouch;
    MidlyPitchBend pitch_bend;
} MidlyMidiMessage;

/**
 * Midi event
 */
typedef struct {
    uint8_t channel;
    MidlyMidiMessageType msg_type;
    MidlyMidiMessage msg;
} MidlyMidiEvent;

/**
 * System common event
 */
typedef struct {
    MidlyInternalSystemCommon *com;
} MidlySystemCommon;

/**
 * System realtime event
 */
typedef struct {
    MidlySystemRealtimeType type_;
    /**
     * Only defined for [SystemRealtimeType::Undefined]
     */
    uint8_t und;
} MidlySystemRealtime;

/**
 * Live event inner
 */
typedef union {
    MidlyMidiEvent midi;
    MidlySystemCommon common;
    MidlySystemRealtime realtime;
} MidlyLiveEventInner;

/**
 * Live event
 */
typedef struct {
    MidlyLiveEventType type_;
    MidlyLiveEventInner inner;
} MidlyLiveEvent;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

/**
 * Create a new [MidiStream]
 */
MidlyStatus midly_stream_new(MidlyAllocOperations alloc_ops, MidlyMidiStream *stream);

/**
 * Drop (and free) a [MidiStream]
 */
void midly_stream_drop(MidlyMidiStream *stream);

/**
 * Feed new data in a [MidiStream]
 */
void midly_stream_feed(MidlyMidiStream *stream,
                       const uint8_t *data,
                       uintptr_t len,
                       void (*handle_ev)(const MidlyLiveEvent*));

/**
 * Write a [MidiEvent] into a buffer
 */
MidlyStatus midly_live_event_write(const MidlyLiveEvent *live_event, uint8_t *buf, uintptr_t len);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif /* MIDLY_C_H */
